---
title: "Guia Final"
author: "Matías Valdés Peña- Matías Vergara Arroyo"
date: "17-08-2020"
output: 
  pdf_document:
    highlight: zenburn
    toc: yes
    number_sections: true
    toc_depth: 2
    latex_engine: pdflatex
  html_document:
    highlight: default
    number_sections: true
    theme: cosmo
    toc: yes
    toc_depth: 2
  word_document: default
---

# Enfermedades cardiacas

### Carga de librería
```{r}
library(ggplot2)
```

### Carga de archivo
```{r}
filename <- "datasets_heart.csv"
data <- read.csv(filename, sep=",")
```

### Funcion que nos permitirá quitar los valores atipicos
```{r}
no_outliers <- function(x, removeNA = TRUE){
  quantiles <- quantile(x, c(0.05, 0.95), na_rm = removeNA)
  x[x<quantiles[1]] <- mean(x, na_rm = removeNA)
  x[x>quantiles[2]] <- median(x, na_rm = removeNA)
  x
}
```
Esta función lo que hace es remover todos los datos que están fuera de los rangos normales por lo tanto, nos removerá todos los datos atípicos que estén fuera de la mayoría de los datos que entregaremos a la función para ser analizados. Esto lo hace a través de dos cuartiles (promedio y mediana) para obtener una gráfica más limpia de los datos sin los datos atipicos. Sin embargo, eliminará los datos o intercambiará por N/A.

### Dimension del dataset 
La dimensión del dataset que se trabajará es de:
```{r}
dim(data)
```

### Nombre de las columnas 
El nombre de las columnas de nuestro dataset son:
```{r}
colnames(data)
```
### Estructura de las variables del dataset
La estructura de las variables de nuestro dataset es la siguiente
```{r}
str(data)
```
### Resumen estadistico
El resumen estadistico del dataset es el siguiente:
```{r}
summary(data)
```

## El dataset será separado en dos grupos, mayores de edad y los menores
### Dataset filtrado por mayores
```{r}
data_mayores <- subset(data, age>18)
summary(data_mayores)
```
### Dataset filtrado por menores
```{r}
data_menores <- subset(data, age<18)
summary(data_menores)
```

### Cambio de la data
```{r}
data$age<- as.numeric(gsub(",", ".",as.character(data$age)))
str(data$age)

data$trestbps<- as.numeric(gsub(",", ".",as.character(data$trestbps)))
str(data$trestbps)

data$chol<- as.numeric(gsub(",", ".",as.character(data$chol)))
str(data$chol)

data$thalach<- as.numeric(gsub(",", ".",as.character(data$thalach)))
str(data$thalach)

data$oldpeak<- as.numeric(gsub(",", ".",as.character(data$oldpeak)))
str(data$oldpeak)

```
Cambiamos el formato de estos datos para un mejor analisis


## A continuacion se representaran diagramas para las variables cuantitativas y lo que significa cada una de ellas

### Graficos de edad

#### Boxplot edad
```{r}
boxplot(data$age, main="Caja de bigotes para la edad", col="green")
```
Se puede apreciar una concentración entre los años 45-65 aproximadamente
#### Mapa densidad edad
```{r}
ggplot(mapping=aes(x = data$age)) + geom_density()
```
En esta representación se observa con mayor claridad la concentración de la edad
#### Boxplot sin outliers
```{r}
age_no_outliers <- no_outliers(data$age)
boxplot(age_no_outliers, main = "Caja de bigotes para la edad sin outliers", col="blue")
```
Acá podemos observar casi sin cambios frente a la primera grafica ya que no existían valores atipicos en la data


### Graficos de trestbps
Nos referimos a trestbps a la presión arterial en reposo
#### Boxplot de trestbps
```{r}
boxplot(data$trestbps, main="Caja de bigotes para trestbps", col="green")
```
#### Mapa de densidad de trestbps
Podemos observar una concentración entre 120-150 pero a la vez se pueden observar valores atipicos
```{r}
ggplot(mapping=aes(x=data$trestbps)) + geom_density()
```
#### Boxplot sin outliers de trestbps
```{r}
trestbps_no <- no_outliers(data$trestbps)
boxplot(trestbps_no, main="Caja de bigotes para trestbps sin outliers", col="blue")
```
En la grafica anterior podemos ver la representación grafica en caja de bigotes sin los datos atipicos que se encontraron en la data
#### Histograma de trestbps 
```{r}
hist(data$trestbps, main="Presion arterial", col="red")
```
Podemos ver en este histograma como los datos de la presión arterial se concentraron y podemos observar pequeños valores atipicos con un pequeño rectangulo luego de la concentración de todos los datos

### Graficos de chol
Nos referimos a chol como al colesterol sérico en mg/dl
#### Boxplot de chol
```{r}
boxplot(data$chol, main="Caja de bigotes para chol", col="green")
```
Se puede observar una concentración completa en un punto pero valores muy exagerados al promedio
#### Mapa de densidad de chol
```{r}
ggplot(mapping=aes(x=data$chol)) + geom_density()
```
Se observa una concentración y luego una baja pero con pequeños montes que nos indican que existen valores extremos
#### Boxplot sin outliers de chol
```{r}
chol_no <- no_outliers(data$chol)
boxplot(chol_no, main="Caja de bigotes para chol sin outliers", col="blue")
```
Data de chol sin los valores atipicos representados en la caja de bigotes
#### Histograma de chol
```{r}
hist(data$chol, main="Colesterol sérico en mg/dl", col="red")
```
Con el histograma presentado se observa claramente la concentracion de los datos y los valores atipicos con el ennegrecimiento de los rectangulos más adelante en los valores


### Graficos de thalach
Nos referimos a thalach como a la frecuencia cardíaca máxima alcanzada por el paciente
#### Boxplot de thalach
```{r}
boxplot(data$thalach, main="Caja de bigotes para thalach", col="green")
```
#### Mapa de densidad de thalach
Se observa una caja de bigotes normal con la excepción de un valor atipico en la muestra
```{r}
ggplot(mapping=aes(x=data$thalach)) + geom_density()
```
La grafica de thalach nos demuestra la densidad de los datos
#### Boxplot sin outliers de thalach
```{r}
thalach_no <- no_outliers(data$thalach)
boxplot(thalach_no, main="Caja de bigotes para thalach sin outliers", col="blue")
```
La grafica no varía según a la primera grafica de bigotes ya que solo existía un valor atipico


### Graficos de oldpeaks
Nos referimos a oldpeaks como a la depresión del ST inducida por el ejercicio relativo al descanso
#### Boxplot de oldpeak
```{r}
boxplot(data$oldpeak, main="Caja de bigotes para oldpeak", col="green")
```
Podemos observar una concentración en el inicio, valores atipicos cercanos y unos muy lejanos a la mayoría de la data
#### Mapa de densidad de oldpeak
```{r}
ggplot(mapping=aes(x=data$oldpeak)) + geom_density()
```
Se observa claramente la concentración al inicio de los datos 
#### Boxplot de oldpeak sin outliers
Los valores atípicos de la variable son:
```{r}
oldpeak_no <- no_outliers(data$oldpeak)
boxplot(oldpeak_no, main="Caja de bigotes para oldpeak sin outliers", col="blue")
```
Esta grafica cambia considerablemente con respecto a la primera ya que podemos apreciar claramente los datos que antes no se podían observar por los valores atipicos que realmente eran exagerados
#### Histograma de oldpeak
```{r}
hist(data$oldpeak, main="Depresión del ST inducida por el ejercicio relativo al descanso", col="red")
```
Con este histograma observamos claramente que los datos están concentrados al inicio y se puede observar un pequeño ennegrecimiento al final que nos muestra que existen esos valores atipicos

## Finalmente, las variables cualitativas las cuales están asociadas a valores para expresar alguna caracteristica en general

### Estadisticas del sexo
#### Plot del sexo
Los hombres fueron representados con el número 1 y las mujeres con el numero 0
```{r}
plot(data$sex)
```
#### Resumen de sexo
Un resumen estadistico de la variable cualitativa es
```{r}
summary(data$sex)
```


### Estadisticas de cp
Cp representa el tipo de dolor en el pecho el cual está clasificado en 4 valores
#### Plot del tipo de dolor en el pecho
```{r}
plot(data$cp)
```
Este grafico nos da una idea de que el tipo de dolor asociado al valor 0 es el que más se presenta en las personas del estudio
#### Resumen de cp
Un resumen estadistico de este tipo de dolor es
```{r}
summary(data$cp)
```

### Estadisticas de fbs
Fbs representa el azucar en sangre en ayunas, para el trabajo de este dataset se asoció el valor 0 para valores menores a 120 mg/dl y el valor 1 para valores mayores a 120 mg/dl
#### Plot de fbs 
```{r}
plot(data$fbs)
```
Con esto podemos concluir que la mayoría de las personas presentaban valores menores a 120 mg/dl de azucar en sangre en las ayunas
#### Resumen
Un resumen de esta variable es
```{r}
summary(data$fbs)
```


### Estadisticas de restecg
El valor restecg se asocio a los resultados electrocardiográficos en reposo, para esta variable se asociaron valores 0,1,2
#### Plot de restecg
```{r}
plot(data$restecg)
```
Con lo que podemos apreciar una concentración en los valores 0 y 1 pero pocas muestras en el valor 2
#### Resumen de restecg
Un resumen de esta variable es
```{r}
summary(data$restecg)
```


### Estadisticas de exang
La variable exang está asociada a la angina inducida por el ejercicio
#### Plot de exang
```{r}
plot(data$exang)
```
Podemos apreciar una mayor concentración del tipo 0 
#### Resumen de exang
Un resumen estadistico de la variable es
```{r}
summary(data$exang)
```

### Estadisticas de slope
Slope corresponde a la pendiente del segmento ST de ejercicio en etapa alta
#### Plot de slope
```{r}
plot(data$slope)
```
Podemos observar una gran concentración en los valores asociados a 1 y 2 pero una baja concentración en los valores asociados a 0
#### Resumen de slope
Un resumen estadistico de la variable es
```{r}
summary(data$slope)
```


### Estadistica de ca
Ca está asociado al número de vasos principales coloreados por flouroscopía, los datos están asociados a números de 0-3
#### Plot de ca 
```{r}
plot(data$ca)
```
Se puede observar claramente una concentración casi completa en los datos asociados al número 0
#### Resumen de ca
Un resumen estadistico de los datos es
```{r}
summary(data$ca)
```

### Estadisticas de thal
Thal está asociado a defectos, los cuales están clasificados en:
1 = defecto reversible
2 = defecto fijo
3 = normal
#### Plot de thal
```{r}
plot(data$thal)
```
Podemos apreciar una concentración en defectos fijos (2) además de una concentración más baja en normal (3) además se pueden observar 3 valores atipicos
#### Resumen de thal
Un resumen estadistico de thal es
```{r}
summary(data$thal)
```


### Estadisticas de target
Target está asociado a ataques cardíacos, el 1 está representado para ataques cardíacos y el 0 para ausencia de ataque cardíaco
#### Plot de target
```{r}
plot(data$target)
```
Podemos observar una tendencia casi igual y casi imposible de deducir si alguna es mayor que la otra
#### Resumen de target
Un resumen estadistico que nos permitirá una mayor comprension
```{r}
summary(data$target)
```

## Eliminación de valores atipicos por NA 
Como no se eliminaron los NA al principio ya que solo se omitieron, en este inciso se eliminarán los valores atipicos o mas bien se reemplazaran por NA
### Eliminación de trestbps
```{r}
outliers <- boxplot.stats(data$trestbps)$out
data$trestbps <- ifelse(data$trestbps %in% 
                          outliers,
                          NA, data$trestbps)

```
### Eliminacion de chol
```{r}
outliers <- boxplot.stats(data$chol)$out
data$chol <- ifelse(data$chol %in% 
                          outliers,
                          NA, data$chol)

```
### Eliminacion de thalach
```{r}
outliers <- boxplot.stats(data$thalach)$out
if(!is.null(outliers)){
  data$thalach <- ifelse(data$thalach %in% 
                            outliers,
                            NA, data$thalach) 
}
```
### Eliminacion oldpeak
```{r}
outliers <- boxplot.stats(data$oldpeak)$out
if(!is.null(outliers)){
  data$oldpeak <- ifelse(data$oldpeak %in% 
                            outliers,
                            NA, data$oldpeak) 
}
```

## Escritura del nuevo dataset
Escritura del dataset con los valores atipicos reemplazados por NA
```{r}
write.csv(data, file="dataset_clean.csv")
```

# Selección de variables cualitativas para analisis
## Abrir data
```{r abrir data}
filename <- "dataset_clean.csv"
data <- read.csv(filename, sep=",")
```

## Carga de librerías
Librerías que serán de ayuda para trabajar en la guía
```{r load library, message=FALSE, warning=FALSE}
library(ggplot2)
library(car)
library(grid)
library(gridExtra)
library(GGally)
library(plyr)
```
## Selección de variables cuantitativas y cualitativas
Para poder realizar la comparación de las variables en los graficos se necesitan seleccionar variables cuantitativas y dos cualitativas, para así realizar la comparación con las dos cualitativas. Para este trabajo se seleccionaron como variables cualitativas el sexo y el fbs que como recordamos del trabajo anterior era la glucemia de ayuna, el 0 para menores de 120 mg/dl y 1 para mayores a 120 mg/dl
### Cuantitativas
```{r}
cuantitativas <- data[, c("trestbps", "chol", "thalach", "oldpeak"), ]
```
### Cualitativas
```{r}
cualitativas<-data[, c("sex", "target", "thal", "fbs", "restecg"), ]
cualitativas$sex[cualitativas$sex==1]<-"Hombre"
cualitativas$sex[cualitativas$sex==0]<-"Mujer"
cualitativas$fbs[cualitativas$fbs==0]<-"Menor a 120 mg/dl"
cualitativas$fbs[cualitativas$fbs==1]<-"Mayor a 120 mg/dl"

```
Se eligen como variables cualitativas el sexo porque es una variable que es facil se utilizar y que la mayoría de la gente comprende rapidamente y además se elige el fbs que es el azucar en sangre en ayunas que es uno de los motivos que se cree importante de analizar

# Graficos

## Histogramas
### Función
Se utilizará una creación de histograma para las variables cuantitativas, por lo que se establecerá una función para usar con las que sean necesarias, así se reduce el código y así se hace más eficiente
```{r funcion histograma}
crea_histograma <- function(column, titulo, variables, w=4, legend=FALSE){
  if(legend){
    show_legend = theme(legend.position = "right")
  }else{
    show_legend = theme(legend.position = "none")
  }
  
  ggplot(data=cuantitativas, aes(column)) +
    geom_histogram(binwidth = w, color = "black", aes(fill=variables)) +
    xlab(titulo) +
    ylab("Frecuencia") +
    show_legend +
    ggtitle(paste("Histograma de", titulo)) +
    geom_vline(data=cuantitativas, aes(xintercept = mean(na.omit(column))),
               linetype="dashed", color = "yellow")
}

```
### Histogramas respecto al sexo
```{r graficos respecto al sexo, warning=FALSE}
trestbps_sex <- crea_histograma(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_histograma(cuantitativas$chol, "Chol", cualitativas$sex, 5)
thalach_sex <- crea_histograma(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_histograma(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, 1, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Cualidad:Sexo", 
                          gp=gpar(fontsize=14)))
```

Como podemos analizar en los 4 gráficos expuestos frente al sexo de los pacientes de las personas, se puede observar tanto en el grafico de trestbps tanto como de thalach la mayoría de los pacientes hombres que se han tomado para el estudio.
Además se observa que en los 3 primeros gráficos la media es cercana a la mitad del intervalo de los datos obtenidos. Trestbps (aproximadamente 130), Chol (aproximadamente 240-260) y thalach (aproximadamente 150). Sin embargo, en el grafico de oldpeak (depresión del ST inducida por el ejercicio relativo al descanso) se observa una clara concentración entre los valores 0-1

### Histogramas respecto a la glicemia en ayunas (fbs)
```{r graficos respecto a la glicemia en ayunas, warning=FALSE}
trestbps_sex <- crea_histograma(cuantitativas$trestbps, "Trestbps", cualitativas$fbs)
chol_sex <- crea_histograma(cuantitativas$chol, "Chol", cualitativas$fbs, 5)
thalach_sex <- crea_histograma(cuantitativas$thalach, "Thalach", cualitativas$fbs)
oldpeak_sex <- crea_histograma(cuantitativas$oldpeak, "Oldpeak", cualitativas$fbs, 1, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Cualidad: Fbs", 
                          gp=gpar(fontsize=14)))
```
Como se observan en los 4 graficos la mayoría de las personas presentaron glicemia en ayunas menor a 120 mg/dl. Además observamos las mismas medias que anteriormente ya que son los mismos datos solo con el cambio de la variable cualititativa en cuestión para analizar.

## Densidad
Un gráfico de densidad visualiza la distribución de datos en un intervalo. 
### Función
```{r densidad}
crea_densidad <-function(column, titulo, variable, legend=FALSE){
  if(legend){
    show_legend =theme(legend.position ="right")
    }else{
      show_legend =theme(legend.position="none")
      }
  
  ggplot(data=cuantitativas, aes(column, color=variable, fill=variable)) +
    geom_density(alpha=.3) +
    xlab(titulo) +
    ylab("Frecuencia") +
    show_legend +
    ggtitle(paste("Diagrama de densidad de", titulo)) +
    geom_vline(data=cuantitativas, 
               aes(xintercept = mean(na.omit(column)),
                   color=variable),
               linetype="dashed", color="gold")
} 
```

### Diagramas respecto al sexo
```{r graficos de densidad sexo, message=FALSE, warning=FALSE}
trestbps_sex <- crea_densidad(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_densidad(cuantitativas$chol, "Chol", cualitativas$sex)
thalach_sex <- crea_densidad(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_densidad(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Cualidad: Sexo", 
                          gp=gpar(dontsize=14)))

```

En el caso de Trestbps el máximo de frecuencia entre ambos sexos es similar y tambien podriamos considerar que la distribucion de densidad tambien lo es, la unica diferencia que podemos apreciar a simple vista es que los hombres poseen una zona media más cercana a la media de los datos además de que es mucho más densa que el de las mujeres que viene despues de la linea de media y es un poco menos densa

En el caso de Chol se observa que los hombres presenta un peak mas alto mientras que en las mujeres se puede apreciar una consistencia la que nos permite deducir que poseen menor tasa de dispersión en los datos

Tanto en Thalach como en Oldpeak se puede observar que no hay simetría en los datos y en ambos los datos de mujer poseen un peak mayor en respecto a los hombres, sin embargo, en thalach podemos apreciar una cierta tendencia en los hombres entre los datos 140-180 aproximadamente.

### Diagramas respecto a la glicemia en ayunos
```{r graficos de densidad fbs, message=FALSE, warning=FALSE}
trestbps_sex <- crea_densidad(cuantitativas$trestbps, "Trestbps", cualitativas$fbs)
chol_sex <- crea_densidad(cuantitativas$chol, "Chol", cualitativas$fbs)
thalach_sex <- crea_densidad(cuantitativas$thalach, "Thalach", cualitativas$fbs)
oldpeak_sex <- crea_densidad(cuantitativas$oldpeak, "Oldpeak", cualitativas$fbs, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Cualidad: Glicemia en ayuno", 
                          gp=gpar(dontsize=14)))

```

En el caso de Trestbps podemos observar una consistencia en el peak de los datos de menores a 120 mg/dl sin embargo, tambien apreciamos una distribución un tanto más simetrica en los datos de mayores a 120 mg/dl 

En el caso de Chol las distribuciones son casi identicas lo que indica una distribución simetrica de ambos datos

En en el caso de Thalach se encuentra una discrepancia entre los datos 90-150 en que los datos no son simetricos entre ambas variables, sin embargo, luego de 150 se puede observar cierta identidad entre ambas variables lo que nos indica cierta correspondencia entre los datos mayores a 150

En en el caso de oldpeak observamos que los datos con menores a 120 mg/dl poseen una frecuencia mayor con respecto a los mayores de 120 mg/dl desde ahí se presenta una identidad casi completa entre ambos datos.

## Boxplot
Un diagrama de cajas y bigotes se usa para mostrar visualmente grupos de datos numéricos a través de sus cuartiles
### Funcion boxplot
```{r funcion boxplot}
crea_boxplot <- function(column, titulo, cualidad,  legend=FALSE){
  if(legend){
    show_legend = theme(legend.position = "right")
  } else{
    show_legend = theme(legend.position = "none")
  }
  
  ggplot(data=cuantitativas, aes(cualidad, column, fill=cualidad)) + 
  geom_boxplot() +
  scale_y_continuous(titulo, breaks=seq(0,30, by=.5)) + 
  show_legend
}
```

### Boxplot con la variable sexo
```{r graficos boxplot sex, warning=FALSE}
trestbps_sex <- crea_boxplot(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_boxplot(cuantitativas$chol, "Chol", cualitativas$sex)
thalach_sex <- crea_boxplot(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_boxplot(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow = 2,
             top = textGrob("Sex boxplot",
                            gp=gpar(fontsize=14)))

```

El Trestbps frente a ambas cualidades son simétricas.

El Chol se puede considerar ambos casos como simétricos, pero en los hombres están más comprimidos hacia el primer cuartil

El Thalach en los datos de los hombres se puede apreciar una cierta simetría porque no la podemos asumir, mientras que en las mujeres encontramos un valor atípico además la simetría es muy compacta pero luego por el valor atipico podemos concluir que la variable posee datos dispersos

El Oldpeak en ambos casos es muy compacto en los primeros valores lo que nos indica que están comprimidos en esas frecuencias sin embargo el estadistico de las mujeres presenta dos valores atipicos

### Boxplot con la variable fbs
```{r graficos boxplot fbs, warning=FALSE}
trestbps_sex <- crea_boxplot(cuantitativas$trestbps, "Trestbps", cualitativas$fbs)
chol_sex <- crea_boxplot(cuantitativas$chol, "Chol", cualitativas$fbs)
thalach_sex <- crea_boxplot(cuantitativas$thalach, "Thalach", cualitativas$fbs)
oldpeak_sex <- crea_boxplot(cuantitativas$oldpeak, "Oldpeak", cualitativas$fbs, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow = 2,
             top = textGrob("Fbs boxplot",
                            gp=gpar(fontsize=14)))

```

En el caso de Trestbps los evaluados con glicemia menor a 120 mg/dl presentan simetría de datos, sin embargo, los que poseen mayor a 120 mg/dl poseen una compresión hacia el primer cuartil. Ambos no poseen valores atipicos
En el caso Chol podemos observar una cierta simetría en el caso de ambos casos sin embargo se puede considerar que los datos menores a 120 mg/dl están mas comprimidos. Ambos no poseen valores atipicos

En el caso Thalach se observa la misma media en ambas cualidades pero su distribución es distinta además podmos observar que los que poseen mayor a 120 mg/dl poseen dos valores atipicos y su caja está mas comprimida en referencia a la de menores de 120 mg/dl

En Oldpeak podemos observar que ambas cajas poseen la misma forma pero varía en la media de ambas ademas de que en los menores a 120 mg/dl se posee un valor atipico

## Violin plot
Utilizados para visualizar la distribución de los datos y su densidad de probabilidad
### Función
```{r violin plot, warning=FALSE}
crea_violin <- function(column, titulo, variables, legend=FALSE){
  if(legend){
    show_legend = theme(legend.position = "right")
  } else{
    show_legend = theme(legend.position = "none")
  }
  
  ggplot(data=cuantitativas, aes(variables, column, fill=variables)) + 
  geom_violin(aes(color=variables), trim = T) +
  scale_y_continuous(titulo, breaks=seq(0,30, by=.5)) +
  geom_boxplot(width=0.1) +
  show_legend
}

```
### Violin plot de la cualidad sex
```{r graficos violin sex, warning=FALSE}
trestbps_sex <- crea_violin(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_violin(cuantitativas$chol, "Chol", cualitativas$sex)
thalach_sex <- crea_violin(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_violin(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow = 2,
             top = textGrob("Sex violin plot",
                            gp=gpar(fontsize=14)))

```

En el caso de Trestbps se puede apreciar una cierta simetría en ambos casos.
En el caso de Chol pdemos apreciar que la densidad de los hombres está mas expandida pero la de las mujeres mas contraídas y ademas podemos observar que los datos en los hombres están mas juntos en cambio en las mujeres están mas simetricamente

En el caso de Thalach podmos apreciar una distribución asimetrica en cambio en las mujeres ademas de que se presenta una zona de mayor densidad de probabilidad además presenta un valor atipico minimo
En el caso de oldpeak ambas cualidad presentan una distribucion tipo piramidal pero la concentracion de los datos en las mujeres es mayor que en los hombres también las mujeres presentan dos valores atipicos mayores

### Violin plot para Fbs
```{r glicemia violin, warning=FALSE}
trestbps_fbs <- crea_violin(cuantitativas$trestbps, "Trestbps", cualitativas$fbs)
chol_fbs <-crea_violin(cuantitativas$chol, "Chol", cualitativas$fbs)
thalach_fbs <- crea_violin(cuantitativas$thalach, "Thalach", cualitativas$fbs)
oldpeak_fbs <- crea_violin(cuantitativas$oldpeak, "Oldpeak", cualitativas$fbs, TRUE)

grid.arrange(trestbps_fbs + ggtitle(""),
             chol_fbs + ggtitle(""),
             thalach_fbs + ggtitle(""),
             oldpeak_fbs + ggtitle(""),
             nrow = 2,
             top = textGrob("Glicemia violin plot",
                            gp=gpar(fontsize=14)))

```
En el caso de Trestbps podemos observar una acumulacion de densidad en los mayores a 120 mg/dl en la zona del primer cuartil en cambio en los menores podmos apreciar una densidad un poco más simetrica
En el caso de chol podemos observar una densidad casi identica para ambos datos pero la media de ambos se localiza en diferentes puntos
En el caso Thalach los pacientes con glicemia mayor a 120 mg/dl tienen una distribución casi simetrica pero los dos valores atipicos menores difieren la simetría en cambio los pacientes con menor a 120 mg/dl poseen una densidad alta en los datos de la mediana y media
En el caso de oldpeak ambas cualidades presentan casi la misma densidad solo que en presentan diferencias en la localizacion de la media además en los pacientes con menor a 120 mg/dl se presenta un peak notorio al inicio de la grafica

## Scatterplot
Los scatterplot usan una colección de puntos en coordenadas cartesianas para mostrar valores de dos variables. Sirven para mostrar si existe relacion entre ambas
### Funcion Scatterplot
```{r scatterplot funcion, warning=FALSE, message=FALSE}
crea_scatterplot <- function(abscisa, ordenada, variable, x, y){
  show_legend=theme(legend.position = "bottom")
  ggplot(data=cuantitativas, aes(x=abscisa, y=ordenada)) + 
    xlab(x) +
    ylab(y) +
  geom_point(aes(color=variable, shape=variable)) +
  geom_smooth(method ="lm") + 
  show_legend  
}
```

### Trestbps - Oldpeak
```{r, warning=FALSE, message=FALSE}
trest_oldpeak_sex <- crea_scatterplot(cuantitativas$trestbps, cuantitativas$oldpeak, 
                                     cualitativas$sex, "Trestbps", "Oldpeak")
trest_oldpeak_fbs <- crea_scatterplot(cuantitativas$trestbps, cuantitativas$oldpeak, 
                                    cualitativas$fbs, "Trestbps", "Oldpeak")

grid.arrange(trest_oldpeak_sex + ggtitle("Sexo"),
             trest_oldpeak_fbs + ggtitle("Glicemia"),
             nrow=1,
             top=textGrob("Trestbps vs Oldpeak",
                          gp=gpar(fontsize=14)))
```
La regresión lineal en ambos casos es la misma

### Trestbps - Chol
```{r, warning=FALSE, message=FALSE}
trest_chol_sex <- crea_scatterplot(cuantitativas$trestbps, cuantitativas$chol, 
                            cualitativas$sex, "Trestbps", "Chol")
trest_chol_fbs <- crea_scatterplot(cuantitativas$trestbps, cuantitativas$chol, 
                            cualitativas$fbs, "Trestbps", "Chol")

grid.arrange(trest_chol_sex + ggtitle("Sexo"),
             trest_chol_fbs + ggtitle("Glicemia"),
             nrow=1,
             top=textGrob("Trestbps vs Chol",
                          gp=gpar(fontsize=14)))
```
Debido a la regresión lineal, podemos observar ciertos patrones en las dos variables además de que podemos ver cierta concentración en las mismas partes en los dos datos es porque analizamos dos variables pero con distintas variables.


### Trestbps - Thalach
```{r, warning=FALSE, message=FALSE}
trest_thalach_sex <- crea_scatterplot(cuantitativas$trestbps, cuantitativas$thalach,
                               cualitativas$sex, "Trestbps", "Thalach")
trest_thalach_fbs <- crea_scatterplot(cuantitativas$trestbps, cuantitativas$thalach, 
                               cualitativas$fbs, "Trestbps", "Thalach")

grid.arrange(trest_thalach_sex + ggtitle("Sexo"),
             trest_thalach_fbs + ggtitle("Glicemia"),
             nrow=1,
             top=textGrob("Trestbps vs Thalach",
                          gp=gpar(fontsize=14)))
```
En este caso la regresión lineal es decreciente y además es más dispersa para ambos gráficos.


### Chol - Thalach
```{r, warning=FALSE, message=FALSE}
chol_thalach_sex <- crea_scatterplot(cuantitativas$chol, cuantitativas$thalach, 
                              cualitativas$sex, "Chol", "Thalach")
chol_thalach_fbs <- crea_scatterplot(cuantitativas$chol, cuantitativas$thalach, 
                              cualitativas$fbs, "Chol", "Thalach")

grid.arrange(chol_thalach_sex + ggtitle("Sexo"),
             chol_thalach_fbs + ggtitle("Glicemia"),
             nrow=1,
             top=textGrob("Chol vs Thalach",
                          gp=gpar(fontsize=14)))
```
La regresión lineal es negativa además los datos están dispersos por lo tanto no se puede asumir ninguna simulitud entre ambos datos

### Chol - Oldpeak
```{r, warning=FALSE, message=FALSE}
chol_oldpeak_sex <- crea_scatterplot(cuantitativas$chol, cuantitativas$oldpeak, 
                              cualitativas$sex, "Chol", "Oldpeak")
chol_oldpeak_fbs <- crea_scatterplot(cuantitativas$chol, cuantitativas$oldpeak, 
                              cualitativas$fbs, "Chol", "Oldpeak")

grid.arrange(chol_oldpeak_sex + ggtitle("Sexo"),
             chol_oldpeak_fbs + ggtitle("Glicemia"),
             nrow=1,
             top=textGrob("Chol vs Oldpeak",
                          gp=gpar(fontsize=14)))
```
La regresión lineal no presenta mayor información solo que en los datos están más dispersos y exite una concentración en el principio.

### Thalach - Oldpeak
```{r, warning=FALSE, message=FALSE}
thalach_oldpeak_sex <- crea_scatterplot(cuantitativas$thalach, cuantitativas$oldpeak, 
                                 cualitativas$sex, "Thalach", "Oldpeak")
thalach_oldpeak_fbs <- crea_scatterplot(cuantitativas$thalach, cuantitativas$oldpeak, 
                                 cualitativas$fbs, "Thalach", "Oldpeak")

grid.arrange(thalach_oldpeak_sex + ggtitle("Sexo"),
             thalach_oldpeak_fbs + ggtitle("Glicemia"),
             nrow=1,
             top=textGrob("Thalach vs Oldpeak",
                          gp=gpar(fontsize=14)))
```
La regresión lineal presenta una pendiente muy negativa lo que concuerda con la dispersion de los datos

## Correlación
Estos gráficos nos enseñan cuando dos elementos presentan independencia uno del otro
```{r, warning=FALSE, message=FALSE}
ggpairs(data=cuantitativas,
        title ="Correlation Cuantitativas",
        upper =list(continuous =wrap("cor",size=5)),
        lower =list(continuous ="smooth"))

```
Se puede concluir que entre los datos del estudio no existe una relación significativa y son independientes por el porcentaje de identidad entre los datos de estudio de las variables cuantitativas que elegimos para el estudio de las dos variables cualitativas

## Heatmap
Los mapas de calor se utilizan para mostrar los  cambios de los datos con el tiempo. La intensidad del color representa que tan grande es el valor de los datos. Es decir, mientras más brillante  sea el color, mayor será el valor y viceversa. 

```{r}
matriz <- as.matrix(cuantitativas)
# Transpuesta para mapa
transpuesta <- t(matriz)[, nrow(matriz):1]
image(1:ncol(cuantitativas),
      1:nrow(cuantitativas),
      transpuesta, 
      xlab ="Datos", 
      ylab ="Variables")
```
La columna 4 presenta los valores mas grandes porque posee el color mas brillante en cambio podemos apreciar que en la columna dos es el lugar mas rojo y mas opaco lo que nos indica que tiene los valores mas pequeños. Sin embargo, en todas las columnas vemos variabilidad en los colores lo que nos indica que los valores varian (datos) porque podmos ver zonas mas opacas (50-70) y zonas mas brillantes (200-250) por ejemplo en la columna 2. Por ello se concluye que los datos varian desigualmente sin poder asociar zonas de crecimiento o decrecimiento en los valores
# Analisis de probabilidades
## Carga de librerías
```{r}
library(caret)
library(e1071)
```

## Funcion probabilidades
```{r}
teorema_bayes <- function(porcentaje, probabilidad){
  suma <- 0.00
  for(i in 1:length(porcentaje)){
    suma <- suma + ((porcentaje[i]) * probabilidad[i])
  }
  calculo <- c()
  for (i in 1:length(porcentaje)){
    divisor <- porcentaje[i] * probabilidad[i]/suma
    calculo <- c(calculo, divisor)
  }
  return(calculo)
}
```

```{r}
databayes<- data.frame(data$sex, data$chol, data$target)

hembra <- subset(databayes, databayes$data.sex == 0)
hembra_conparo <- subset(hembra, hembra$data.target == 1)
hembra_cholbajo <- subset(hembra_conparo, hembra_conparo$data.chol <= 250)
print(hembra_cholbajo)



```

